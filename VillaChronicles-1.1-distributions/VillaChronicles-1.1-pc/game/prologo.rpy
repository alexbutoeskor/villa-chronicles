label prologo:
    "Viernes 30 de Noviembre"
    "¿Qué día es hoy?"
    "ah, ya recordé, es el día de mi graduación."
    "El ambiente está muy animado, todos mis compañeros de clase al igual que yo tenemos nuestra toga y birrete."
    "La sonrisa en sus rostros es igual a la de un niño abriendo su regalo de navidad."
    "Pero en mi caso."
    "A pesar de que comparto sus sonrisas."
    "No siento lo que se supone que deba sentir."
    "¿Por qué...?"
    return
