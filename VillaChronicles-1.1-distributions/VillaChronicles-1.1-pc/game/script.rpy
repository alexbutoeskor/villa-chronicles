﻿# Coloca el código de tu juego en este archivo.

init:
    $ puntos = 0

##

screen experiencia:
    frame xpos 0 ypos 0:
        text "Puntos de experiencia: [puntos]" xalign 0.1 yalign 0.1

# Declara los personajes usados en el juego como en el ejemplo:

define DV = Character("Yo")
define L  = Character(_("Laura"), color="#0000FF")
define CN = Character(_("Chica Nueva"), color="#0000FF" )
define M  = Character(_("Maria"), color = "#FF00FF")
define J  = Character(_("Jonathan"), color = "#E58700")
define C  = Character(_("Carolina"), color = "#800080")
define MI = Character(_("Milena"), color = "#A326F0")

define prologo = Character(None,window_yfill=True,window_xmargin=20,window_ymargin=30)

image Maria_png = "Tsukishiro.kohaku.png"
image Jonathan_png = "Yamabuki.Shou.png"
image Laura_png = "Tsukishiro.Hitomi.png"
image Carolina_png = "Kazano.Asagi.png"
image Milena_png = "Kawai.Kurumi.png"

# El juego comienza aquí.

label start:

    # Muestra una imagen de fondo: Aquí se usa un marcador de posición por
    # defecto. Es posible añadir un archivo en el directorio 'images' con el
    # nombre "bg room.png" or "bg room.jpg" para que se muestre aquí.

    scene negro

    # Muestra un personaje: Se usa un marcador de posición. Es posible
    # reemplazarlo añadiendo un archivo llamado "eileen happy.png" al directorio
    # 'images'.



    # Presenta las líneas del diálogo.

    call prologo from _call_prologo

    scene escuela_a

    "Lunes 29 de Enero"
    "\"¡Por fin llegue a \"once\"! ¡Hurra!\""
    "Eso me gustaría gritar, pero no es así, a pesar de ser un estudiante promedio tirando a sobresaliente."
    "Realmente no aspiro a nada importante en mi vida, por eso no quería llegar hasta aquí."
    "Pero tampoco puedo dejar que mi mamá se enfade conmigo por ser un vago."
    "Así que solo dejo que los días pasen al ritmo de los demás."
    "Es mi último primer día de clases en el colegio, es igual que todos los anteriores."
    "El discurso del rector repitiendo la carreta de todos los años, la repartición de los grupos, el horario de clases, total a nadie le importa este día."
    "Como sea, de tres grupos me enviaron al \"11-03\"."
    "Si a ese donde están algunos repitentes."
    "Los vagos que consiguieron el milagro de aprobar el año anterior."
    "Uno que otro estudiante nuevo y un puñado de alumnos regulares, entre esos yo, va a ser un año difícil."

    scene salon_b
    with dissolve

    "Ya en el salón de clases nadie a excepción de los nuevos, dos para ser exactos se presentó."
    "Casi todos nos conocemos, así que sabemos con quienes vamos a tratar."
    "No me interesa socializar mucho y menos cuando falta un año para salir de aquí."
    "Pero, bajo estas circunstancias en donde toca hablar con alguien o pierdes es mejor entablar conversación, así que:"

    show screen experiencia with dissolve

    $ puntos += 1

    menu:

        "¿Debería hablar con alguien conocido? o mejor espero a que asignen puestos de estudio:" #(Primera Decisión):

        "¿Debería hablar con alguien conocido?":

            jump conversacion_1

        "Mejor espero a que asignen puestos de estudio":

            jump reanudar_1

label conversacion_1:

    hide screen experiencia with dissolve

    "Hablaré con Jonathan, le gusta el fútbol, no creo que me aburra:"

    show Jonathan_png
    with dissolve

    DV "¿Qué más Jonathan? ¿Qué le parece el nuevo grupo?"
    J "Bien David. Pues normal, igual que el año anterior supongo."
    DV "¿Piensa formar un nuevo equipo para el torneo este año?"
    J "Quizás, me gustaría verlos a todos primero, aunque sé que David juega bien. Que dice: ¿Le gustaría jugar?"
    DV "Tal vez después, quisiera concentrarme en mis estudios por ahora."
    J "Vale, pero jugará después cuando tenga el equipo."
    DV "Sí, sí, yo le prometo jugar."

    hide Jonathan_png
    with dissolve

label reanudar_1:

    hide screen experiencia with dissolve

    "Pasaron veinte minutos a que el profesor Rodríguez entrará al salón, llama al orden y comienza a llamar asistencia."
    "A medida que iba llamando a alguien le iba asignando su puesto."
    "En otras palabras, el clásico orden de lista de estudiantes."
    "Para mi buena suerte, obtuve un lugar al lado de la ventana y al lado mío uno de los dos nuevos."
    "Es una chica, así que tampoco creo que tenga problemas con eso."
    "Después de revisar el programa de trabajo, el profesor nos deja salir al descanso."

    image transicion_1:
        "pasillo_exterior_a" with dissolve
        pause 2.0
        "pasillo_exterior_b" with dissolve
        pause 2.0
        "pasillo_exterior_c" with dissolve
        pause 2.0
        repeat

    show transicion_1

    "Apenas son las nueve, dos horas para no hacer nada."
    "No tengo amigos con quien juntarme así que mejor doy vueltas por ahí, seguro pasa algo bueno."
    "No, no pasó nada, y apenas son las nueve y media."
    "Me dio hambre, iré a comprar algo a la cafetería."

    scene cafeteria_escolar_a
    with dissolve

    "Al llegar veo a María, una chica con la que he compartido clase durante los últimos ocho años."
    "Normalmente estaría con sus amigas, pero está sola, me pregunto qué habrá pasado:"

    show Maria_png
    with dissolve

    DV "Hola María, que más, ¿bien?"
    M "Sí, estoy bien."
    DV "¿Por qué sola?"
    M "Pues..."
    M "Perdí una apuesta con Adriana y con Johana y tengo que invitarles algo."
    DV "¿Qué apostaron?"
    M "Tonterías sobre nosotras."
    DV "Bueno, no preguntaré más sobre eso."
    DV "¿Triste porque las separaron?"
    M "No, algo nos inventamos."
    M "David, usted que sabe..."
    M "¿Por qué Jonathan no quiso cambiarse de curso cuando el profesor de química se lo ofreció?"

    show screen experiencia with dissolve

    $ puntos += 1

    menu:

        "¿Qué debería contestar?" #(Segunda Decisión):

        "Fue porque no quiere saber sobre el director de grupo":

            jump si

        "No sé":

            jump no_se

label si:
    hide screen experiencia with dissolve

    DV "Quizás fue por lo que paso el año pasado en mayo."
    M "¿Por lo del chicle?"
    DV "Sí, pero Jonathan también tiene la culpa."
    DV "Todavía no me explico cómo fue que se le ocurrió pegar dos modelos de moléculas con un chicle."
    M "A mí me dio risa como el profesor se puso."
    M "Nunca lo había visto así de rojo."
    DV "Bueno, eso si no te lo niego."
    M "Vale David, gracias, te veré después en clase."

    hide Maria_png
    with dissolve

    "Después de despedirme de María, compré una barra de granos de avena con chocolate y seguí con mi camino."

    jump continuar

label no_se:

    hide screen experiencia with dissolve

    DV "La verdad no sé, él no me ha dicho algo."
    M "¿Podrías preguntarle por mí?"
    DV "Veré que puedo hacer."
    M" Vale David, gracias de todos modos, te veré después en clase."

    hide Maria_png
    with dissolve

    "Después de despedirme de María, compré una barra de granos de avena con chocolate y seguí con mi camino."

label continuar:

    scene terraza_a
    with dissolve

    "Después de pasados cuarenta minutos me aburro de dar vueltas y voy a la terraza a ver lo que me alcance la vista sobre el barrio."
    "Quizás vea algo interesante, pero pasados quince minutos no veo nada interesante aparte del tráfico en la calle."
    "Mejor me voy a la puerta del salón a esperar a que llegue el profesor."

    scene pasillo_interior_a
    with dissolve

    "Al llegar, veo que no soy el primer obsesionado con que se acabe el descanso."
    "Sentada en el piso, recostada contra los casilleros se encuentra una chica."
    "Precisamente con la que me asignaron compartir puesto."
    "Está jugando con su teléfono."
    "Por lo que mejor no molesto y tomo asiento en el suelo contra la puerta del salón apenas a cierta distancia de ella."

    show telefono_celular
    with dissolve

    "Saco mi teléfono, me coloco mis audífonos y comienzo a escuchar música."
    "Algo de rock no está mal, quizás también algo de metal sería suficiente por ahora."
    "No sé porque,"
    "a pesar de pasar varios minutos escuchando varias canciones no consigo relajarme."

    hide telefono_celular
    with dissolve

    "La presencia de aquella chica me pone incomodo."
    "No es que sea fea, por el contrario, es muy linda."
    "Pero esa expresión tan vacía que tiene en su rostro no me deja apartar la mirada."
    "Igual no pienso hablarle."
    "En algún momento uno tendrá que hablarle al otro por o para cualquier cosa, así sea algo académico."
    "Así que mejor intento fingir que no la he visto y me concentro en mi música."
    "A falta de unos minutos para el final del descanso, comienzan a llegar algunos de mis compañeros de clase."
    "Aquella chica guarda su teléfono, se levanta del suelo mientras se limpia su falda y finge que no estuvo haciendo algo."
    "Yo hago lo mismo y me quedo al lado de la puerta:"
    "Valla forma de perder el tiempo en una hora."
    "A las once de la mañana en punto:"
    "Aparece el profesor Rodríguez con un montón de documentos en la mano."
    "Dios quiera que no sea trabajo."
    "Probablemente sea el observador de cada uno de nosotros."
    "Si es eso no me preocupo, suelo ser tan desapercibido que puedo faltar un día y nadie se da cuenta de que falte."
    "A no ser que llamen asistencia ese día."
    "Como sea entremos a clase a ver qué pasa."

    scene salon_b
    with dissolve

    "Ya adentro, el profesor vuelve a pasar asistencia:"
    "\"Que intenso\" Es lo único que se me viene a la mente."
    "Después de terminar, nos pasa de adelante hacia atrás los documentos que traía en la mano."
    "Tenía razón, era nuestro observador, pero la hoja solo tenía nuestro nombre:"
    "Profesor Rodriguez" "Por favor cada quien complete su hoja del observador con sus datos."
    "Profesor Rodriguez" "Para eso no necesitan hablar, no quiero a nadie de pie, tienen media hora."
    "Me pregunto qué bicho le picó."
    "Él por lo general no suele ser tan mandón con nosotros."
    "Seguramente sea por lo mal que les fue en el examen nacional a los del año anterior."
    "Sea cual sea el motivo, comenzó el año de manera muy estricta."
    "Luego de eso pasaron cinco minutos."
    "Solo era llenar nuestros datos y uno no se demora media hora en diligenciar esos documentos."
    "Así que simplemente me puse a ver por la ventana."
    "Pero siento que me llaman apretando mi brazo derecho."

    show Laura_png
    with dissolve

    "Es la chica nueva con la que me asignaron al iniciar la jornada."
    "Sin decir nada, me pasa sobre la mesa un cuaderno en donde tenía escrito:"
    CN "\"¿El profesor es así de estricto siempre?\""
    "Al leer eso, tomo mi lápiz y escribo debajo de su pregunta:"
    DV "\"Para nada, a saber que le dio.\""
    "Y así comenzó una pequeña charla con mensajes escritos."
    "Mientras uno escribe, el otro vigila al profesor para que no nos descubra:"
    CN "\"¿Entonces no tengo nada de qué preocuparme?\""
    DV "\"No.\""
    CN "\"Vale. ¿Llevas estudiando aquí desde hace mucho?\""
    DV "\"Sí, desde preescolar.\""
    CN "\"Ya veo. ¿Y no has pensado cambiar de colegio?\""
    DV "\"Para nada, además este es el último año, y siendo así menos.\""
    DV "\"¿Preguntas por qué eres nueva?\""
    CN "\"No, solo que estoy aburrida.\""
    CN "\"Dime: ¿Aquí todos como son?\""
    DV "\"Pues normales creería yo.\""
    DV "\"Nadie molesta a nadie y todos por lo menos aquí en este salón son chéveres.\""
    DV "\"No dudo en que hagas amigas rápido.\""
    CN "\"Entiendo. Está lindo el uniforme.\""
    CN "\"Me gusta el negro, pega con todo.\""
    DV "\"A mí también, el buzo por lo que es de lana ayuda mucho para el frio de la mañana.\""
    DV "\"Pero cuando hace sol es insoportable.\""
    CN "\"Lo tendré presente.\""
    CN "\"Dime: ¿Qué tan exigentes son aquí?\""
    DV "\Pues... Yo diría que lo normal como en cualquier colegio público.\""
    DV "\"Si eres buena estudiante no creo que tengas problemas para pasar el año.\""
    CN "\"Menos mal.\""
    CN "\"No quería pasar todo el año pensando en trabajos extensos ni en proyectos absurdos.\""
    CN "\"Estoy cansada de todo eso.\""
    DV "Te entiendo, yo también estoy aburrido de todo eso.\""
    DV "\"Aun así no sé si este año nos colocarán a hacer algo, pero creo que no.\""
    CN "\"Vale, entonces estaré preparada.\""

    hide Laura_png
    with dissolve

    "De un momento a otro, el profesor rompe el silencio y toma palabra:"
    "Profesor Rodriguez" "Bien, creo que ya terminaron, por favor pasen de atrás hacia adelante sus observadores."
    "Después de pasar las hojas, el profesor aprovecha el tiempo que queda para hablarnos de lo que nos enseñará este año;"
    "Las matemáticas son aburridas, pero lo bueno que tiene él es que estas no se sienten tan pesadas."
    "Luego de esa larga introducción termina el primer día."
    "Algunos salen rápidamente del aula con cara de angustia por lo que se viene."
    "Otros con cara de confianza y uno que otro con cara de sueño."
    "Mientras todos se aglutinan en la puerta para salir, yo espero un poco."
    "No es que tenga que hacer algo importante después de salir de este agujero, así que no tengo afán."

    scene pasillo_exterior_b
    with dissolve

    "Al salir del salón de clases, me dirijo a recoger a mi hermana Carolina."
    "Ella va en noveno grado de bachillerato, así que tengo que buscarla a su salón y después nos vamos para nuestra casa."
    "Vivo con ella y con mis padres que a esta hora del día están trabajando."
    "Una bonita familia podría decirse."
    "Pero a nuestros padres solo los vemos por la mañana porque pasan todo el día trabajando."
    "Así que la mayor parte del tiempo convivimos en la casa ella y yo."

    scene salon_a
    with dissolve

    "Al llegar a su salón, me la encuentro despidiéndose de una de sus amigas:"

    show Carolina_png at right

    show Milena_png at left

    C "Chao \"Mile\". Nos vemos mañana."
    "Milena" "Cuídate."

    hide Milena_png
    with dissolve

    show Carolina_png at center
    with move

    "Después de que Milena se marcha, le dirijo la palabra a Carolina:"
    DV "¿Listo? ¿Ya se pusieron al día?"
    C "Todavía no, el tiempo no alcanza."
    DV "¿Tuvieron casi toda la mañana y aun así no les alcanzó el tiempo?"
    C "No, Milena me estaba contando de sus vacaciones en el mar y de lo bien que lo paso, además…"
    DV "Mejor vámonos y me cuentas el resto por el camino."

    scene calle_residencial_dia_a
    with dissolve

    "Durante el trayecto, Carolina me cuenta con minucioso detalle el día que tuvo hoy."
    "A mí no es que me interese mucho."
    "Pero debido a que no tengo nada más importante en que pensar le presto atención."
    "Así ha sido estos últimos años, por lo que ya se quiénes son sus amigas, qué le gusta a cada una y a quien le gusta tal chico."
    "Suele ser aburrido a veces, pero es mi hermana, ella me quiere y no puedo ser distante con ella."

    scene interior_hogar_b
    with dissolve

    "Al llegar y entrar, dejamos las maletas, y pasamos a la cocina."
    "Preparo algo rápido para el almuerzo, no hay mucho en la nevera."
    "Por lo que serán huevos revueltos con algo de pan y jugo, parece otro desayuno, pero no hay más."
    "Después de comer mi hermana se prende del televisor a ver lo que le gusta ver y yo me voy derechito al computador a jugar."

    scene calle_comercial
    with dissolve

    "Luego de varias horas tengo que ir a la tienda de la esquina a comprar algo para preparar algo de comer con el billete de diez que mis padres dejaron."
    "No es mucho, será arroz, un plátano verde y alverjas."

    scene interior_hogar_b
    with dissolve

    "Después de volver de la tienda, cocino y preparo la cena para todos."
    "Sirvo la parte de mi hermana y la mía y dejo lo de mis padres en las ollas."

    scene cocina
    with dissolve

    "Al terminar de comer ayudo a Carolina a lavar los platos."
    "No sé por qué, pero el solo sonido del agua del lavaplatos me pone algo inquieto."
    "Pero no sé de qué hablar con ella."
    "Tal vez debería dejar que ella tome iniciativa y continúe con la conversación de esta tarde."
    "O debería preguntar qué paso aparte de lo que me conto esta mañana."

    show screen experiencia with dissolve

    $ puntos += 1

    menu:

        "¿Que debería hacer:" #(Tercera Decisión)

        "Tomar la iniciativa":

            jump iniciar

        "Dejar que ella hable":

            jump dejar_iniciar

label iniciar:

    hide screen experiencia with dissolve

    show Carolina_png
    with dissolve

    DV "Y bien… ¿Qué pasó después de que Milena y tú fueran al baño?"
    C "Cierto, después de que Milena y yo salimos del baño nos encontramos con Carlos,"
    C "él se puso más lindo por lo que Milena se puso roja y no supo de qué hablarle."
    DV "¿Qué hiciste tú?"
    C "Yo si le hablé, le pregunté qué había hecho en vacaciones."
    DV "¿Y después de que él se fue que paso?"
    C "Milena estaba súper sonrojada, parecía que le iba a dar fiebre."
    C "Yo si estaba bien."
    DV "Hablando de eso… ¿Viste a Fernando hoy?"
    C "No, no lo vi, quería verlo."
    DV "Pero donde hubiera ido, seguramente te habrías puesto más roja que Milena."
    C "Que va, yo no soy así… bueno un poquito."
    C "Como sea, luego de lo de Carlos nos fuimos a clase y casi que nos da sueño."
    DV "¿Por qué?"
    C "Porque la profesora Sandra se puso a hablar y hablar de su vida."
    DV "Sí, ella suele hacer eso, me acuerdo que esas clases de español daba mucho sueño cuando se desviaba del tema."
    C "Sí, ahora tu turno."

label dejar_iniciar:

    hide screen experiencia with dissolve

    C "¿Paso algo interesante en tu último primer día? ¿Por fin te enamoraste de María?"
    DV "No, no pasó nada interesante, lo mismo de siempre,"
    DV "y no, no pasará nada con María,"
    DV "que yo la haya invitado aquí de vez en cuando a hacer trabajos no significa que me guste."
    "Luego de que dijera eso, terminamos de lavar los trastos."
    DV "Bien, terminamos son las nueve,"
    DV "será mejor que nos vallamos a dormir o si no mi papá nos arrancará la cabeza por estas despiertos hasta tan tarde."
    C "Sí, pero esta charla no termina aquí, me contarás como es que conquistas a María."
    DV "Sí, sí, como sea, a dormir."

    scene interior_hogar_b with dissolve

    "Desde que invite a María a la casa a hacer un trabajo de inglés mi hermana no deja de molestarme con eso."
    "Suele ser irritante a veces, pero ya después de varios años con lo mismo ya no me molesta tanto como los primeros días."
    "En fin, ella parará cuando consiga una novia o me valla de la casa."
    "Ahora que lo pienso, es más probable que primero pase lo segundo, mejor para mí."

    scene negro with dissolve

    "Viernes 9 de Febrero"

    scene escuela_a

    "Después de dos semanas de clase, ya el grupo en que me asignaron ya comienza a formar sus grupos de amigos."
    "Jonathan ya formó uno de los dos equipos que representará al curso en el torneo colegial y me tiene como reserva."
    "No sé cómo, pero Johana y Adriana se las arreglaron para cambiarse a nuestro curso por otros dos chicos para poder seguir charlando con María."
    "(Como si no tuvieran los descansos)"
    "Ahora somos doce chicos y veintiocho chicas."
    "Va a ser un año complicado en cuanto a decisiones de grupo."
    "Y por mi parte sigo con mis asuntos."

    scene escuela_c
    with dissolve

    "La única novedad que he tenido este año ha sido Laura, la chica con la que me asignaron a compartir pupitre."
    "Esa chica es un completo misterio."
    "A pesar de su linda apariencia ha mostrado todo lo contrario a como se ve."
    "No habla con nadie, y al parecer no está interesada en eso, solo habla cuando un profesor le pide la palabra."
    "No ha hecho amigas, no sonríe, parece una máquina y esa actitud de \"Muérete\" es suficiente para ahuyentar a cualquiera."
    "Por lo que ya se hizo enemiga de la mayoría de las chicas de la clase."
    "Pero lo que me parece más extraño es que solo se entiende conmigo por medio de notas escritas."
    "A saber si es porque soy con quien comparte el puesto."
    "Aun así no ha sido molestia para mí y por lo que nos hemos escrito, ella esta cómoda conmigo."

    scene salon_b
    with dissolve

    "Es el segundo bloque de clases, porque tenía que ser filosofía antes del descanso, es insufriblemente larga."
    "Él profesor está leyendo un libro en voz medio alta y con esa voz de locutor característica que tiene,"
    "hace que a la mayoría le dé sueño y con este sol de media mañana peor."
    "Afortunadamente ya estaba preparado para una situación así por lo que planee con Laura conversar sobre lo que sea:"

    show Laura_png
    with dissolve

    DV "\"...entonces es por eso que el profesor Jaime no se la lleva con Jonathan.\""
    L "\"No lo puedo creer, en serio... ¿Con chicle?\""
    DV "\"Sí.\""
    L "\"Increíble, ¿Y no han pasado más cosas, así como la que me acabas de contar?\""
    DV "\"No que yo recuerde en este momento, aun así, deben haber pasado,\""
    DV "\"pero por lo general procuro no involucrarme en este tipo de situaciones.\""
    L "\"Entonces es por eso que permaneces solo en los descansos.\""
    DV "\"Más o menos, aunque eso es algo que debería preguntarte yo, ¿En dónde te metes?\""
    L "\"En la biblioteca, nadie va por allá, puedo estar tranquila.\""
    DV "\"¿Entonces eres de las que leen?\""
    L "\"No, voy es a jugar en el teléfono.\""
    DV "\"¿Es enserio?\""
    L "\"Al menos no soy de esos que caminan dando vueltas por los pasillos o por el parque, o las canchas deportivas.\""
    DV "\"En lo que vamos de clases y lo que me he escrito contigo,\""
    DV "\"eres una persona bastante agradable a pesar de esa actitud que demuestras, ¿Haces eso por algo en especial?\""
    L "\"Tengo mis razones.\""

    hide Laura_png
    with dissolve

    "Luego de que ella escribiera eso el profesor interrumpe su lectura señalando el final de la clase:"
    "Profesor" "Pueden buscar el libro que les leí en la biblioteca, nos vemos el siguiente viernes, pueden irse."
    "Laura se despide con su mano y luego se marcha mezclándose con la muchedumbre en la entrada."
    "Por mi parte espero a que todos descongestionen la puerta para poder salir."
    "Mientras recogía mis cosas, Jonathan se acerca a mi puesto con una cara de preocupación:"

    show Jonathan_png
    with dissolve

    DV "¿Ahora que hizo Jonathan?"
    J "Nada que yo sepa, necesito hablar con usted de algo que puede ser importante."
    "Normalmente Jonathan suele ser un payaso y hacer tonterías en clase."
    "Pero la forma con la que me habló me da a entender que es algo serio."
    "En lo personal me encanta el fútbol y sobretodo jugarlo."
    "Pero hoy no tengo ánimos de darle patadas a un balón, ojalá quiera hablarme de otra cosa:"
    DV "Bien, camine me acompaña a la cafetería a comprar algo y después me cuenta de lo que quiere hablar."

    scene terraza_a with dissolve

    "Luego de comprar las barras de cereal que me gustan y salir de la cafetería, Jonathan me lleva hasta la terraza."
    "Aquí no hay mucha gente de nuestro grado por lo que me da a entender que quiere hablar en privado:"

    show Jonathan_png
    with dissolve

    DV "Jonathan, esa cara suya me está preocupando ¿De qué quiere hablar?"
    J "Es sobre María."
    "\"Ahora si me preocupo más.\""
    DV "Qué pasa con ella?"
    J "¿Usted qué piensa de ella?"
    DV "Pues... que es alegre, amable y se lleva bien con todos, además de ser buena persona y  muy linda."
    DV "¿Qué más quiere que le diga?"
    J "¿No piensa nada más de ella?"
    DV "Aparte de que la conozco desde hace ya bastante tiempo y se cómo es, no, nada más."
    J "¿Seguro que nada más?"
    DV "¡Sí, seguro que nada más!"
    DV "¡¿Qué pasa con ella?!"
    J "Es... Lo que pasa es que..."
    DV "¡¿Qué paso?!"
    J "Es que a mí me gusta María."
    DV "..."
    DV "A ver, a ver, a ver, otra vez."
    J "No lo voy a repetir."
    DV "Entiendo eso, pero no entiendo porque me lo cuenta."
    J "Pensé que a usted también le gustaba."
    DV "Para nada, si, es linda y todo, pero no, no siento nada especial por ella."
    "Después de que respondiera eso, el rostro de Jonathan mostro algo de alivio."
    J "Me alegro, me quite la mitad de la carga."
    DV "Me alegro por usted que le guste María."
    DV "Pero ahora que me conto lo que siente por ella me imagino que quiere que haga algo."
    J "Más o menos, quiero llegar a ella por mis propios medios, pero no sé con seguridad lo que ella busca."
    J "Por eso quiero que me ayude, en eso pensaba si no habría problemas."
    J "Además usted la conoce bien y sabe que cosas le gustan."
    DV "¿Por qué me pide ayuda? Yo nunca he tenido novia."
    DV "Así que no tengo experiencia para aconsejarle, hubiera hablado mejor con Juan Carlos."
    J "Sí, pero lo que pasa con él es que es muy sinvergüenza con las chicas y no quiero que María me odie."
    J "Además, David es el más maduro de todos nosotros y tampoco se lo contaría a alguien más porque si."
    J "Por lo que tengo más confianza en usted."
    "En lo personal no quería estar metido en problemas o involucrarme muy a fondo en los asuntos de los demás."
    "Pero a Jonathan le debo un favor enorme por ser el único que me apoyo hace dos años por ese incidente así que…"
    DV "Bien, le ayudaré, puede que tenga chances si deja a un lado la payasada en clase y se porta serio de a poco de ahora en adelante."
    J "Vale, haré lo que sea."
    DV "El verdadero problema no es María en sí, son los dos guardaespaldas que tiene como amigas, así que no le prometo nada."
    J "No importa, como le digo, lo que sea."
    DV "Por el momento, ya que usted está detrás de ella, trate de preguntarle cosas sobre la clase."
    DV "Pero nada más o si no Adriana o Johana se darán cuenta, y deje de hacer chistes infantiles."
    J "Lo tendré presente, y gracias. Más tarde cuadramos lo del torneo. Nos vemos."

    hide Jonathan_png
    with dissolve

    "Luego de que Jonathan se fuera, decido quedarme aquí en la terraza."
    "No quiero cruzarme más con alguien y saber de cosas complicadas."
    "Va a ser un año muy largo por la cantidad de trabajo que tenemos que hacer y ahora esto de Jonathan que lo hace peor."
    "Así qué procuraré no meterme o dejar que me involucren en más asuntos así."

    scene escuela_b with dissolve

    "Para la última hora en educación física, al profesor se le dio la brillante idea de ponernos a hacer trabajos en parejas."
    "Y no sé si fue para mí fortuna o para mi desgracia el tener que trabajar con Laura."
    "Es agradable trabajar con una chica, pero no me gusta llamar mucho la atención y más si la mayoría de las chicas no me ve con muy buenos ojos."
    "Por lo que intentaré ignorar sus miradas y poner mi esfuerzo en concentrarme."
    "Como ya he mencionado antes, Laura no habla con nadie, solo conmigo por medio de notas escritas y no he querido preguntarle porque."
    "Pero para esta clase no tiene más remedio que usar su voz:"

    show Laura_png with dissolve

    L "Entonces… ¿A Jonathan le gusta María?"
    "El tono suave que uso para que nadie más escuchara eso más el contenido de la pregunta hizo que me sorprendiera."
    "Y no es que pueda negarlo, no ganaría nada con hacerlo."
    "Así que con el mismo tono de voz mientras damos vueltas alrededor de la chancha le respondo:"
    DV "¿Cómo lo sabes?"
    L "Entonces si es verdad."
    "Parece que le acabo de despejar las dudas:"
    DV "Repito, ¿Cómo lo sabes?"
    L "Es fácil deducir eso, el modo en el que él la mira es una señal muy obvia."
    L "Además, el que él haya querido hablar contigo con ese rostro me hizo sospecharlo aún más."
    DV "¿Alguien más lo sabe?"
    L "No creo, todos aquí son muy despistados."
    DV "Y ahora que lo sabes ¿Qué piensas hacer?"
    L "Nada, solo quería confirmarlo."
    "Al parecer Laura es una chica muy lista y astuta."
    "Se dio cuenta de todo en un par de semanas."
    "Tendré que no ser tan abierto con ella."
    L "Y bien ¿Le dijiste que le vas a ayudar?"
    "“Esta chica es increíble”"
    DV "La verdad él dijo que quería lograrlo por sus medios, así que solo quiere mi asesoría por así decirlo."
    L "Será mejor que lo aterrices lo más pronto posible."
    L "El no llegará a ninguna parte."
    DV "¿Por qué me lo dices?"
    L "Porque ella no está dispuesta a tener algo con alguien, se le ve en la cara."
    DV "Yo a él le debo un enorme favor."
    L "Entonces devuélvele ese favor con lo que te digo, será lo mejor para todos."
    DV "Bien, veré que puedo hacer."

    scene negro
    with fade

    "Creo que me metí en más de un problema sin buscarlos, no sé si ayudar a Jonathan o escuchar a Laura."

    show screen experiencia with dissolve

    $ puntos += 2

    menu:

        "Deberia creer en Jonathan o escuchar la sugerencia de Laura" #(Primera Decisión Importante)

        "Ayudar a Jonathan":

            jump ayuda_jonathan

        "Escuchar a Laura":

            jump escucha_laura

label ayuda_jonathan:

    hide screen experiencia with dissolve

    scene negro

    "Ojalá lo que decidí sea lo mejor para mí, no quiero más problemas."

label escucha_laura:

    hide screen experiencia with dissolve

    scene negro

    "Ojalá lo que decidí sea lo mejor para mí, no quiero más problemas."

    # Finaliza el juego:

    return
